FROM hoosin/alpine-nginx-nodejs:latest

COPY --from=instana/aws-fargate-nodejs:latest /instana /instana
RUN /instana/setup.sh
ENV NODE_OPTIONS="--require /instana/node_modules/@instana/aws-fargate"

WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
EXPOSE 3000

RUN mkdir -p /etc/nginx
ENV NGINX_CONF /etc/nginx/
RUN cp -r nginx/* $NGINX_CONF
RUN ls
RUN chmod +x ./start_docker_container.sh
CMD ["./start_docker_container.sh"]
