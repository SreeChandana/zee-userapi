const { v4: uuidv4 } = require("uuid");
const dynamoDB = require("../../connections/dynamoConnection");
const config = require("../../config/config");
const favoritesValidation = require("./favoritesValidation");
const tableName = config.favorites.table;
const indexName = config.favorites.index;
const logger = require('../../helpers/logger')
// TODO: get UserId from Auth token

exports.addFavorites = async (req, res) => {
    await favoritesValidation.validateInputs(req, res);
    await favoritesValidation.validateAssetType(req, res);
    await favoritesValidation.validateAssetTypeWithAssetId(req, res);

    let data = await isItemExists(req);

    if (data.Items.length > 0) {
        let error = new Error();
        error = {
            message: "Item Already exists",
            statusCode: 2411,
            status: 404,
        };
        logger.error(JSON.stringify(error));
    }

    const params = {
        TableName: tableName,
        Item: {
            id: uuidv4(),
            userId: req.headers.userid,
            AssetId: req.body.id,
            AssetType: req.body.asset_type,
            Duration: req.body.duration,
            Date: req.body.date,
            createdAt: Date.now(),
        },
    };

    result = await dynamoDB.addDynamo(params);
    return result;
};

exports.getFavorites = async (req, res) => {
    const params = {
        TableName: tableName,
        IndexName: "user_index",
        KeyConditionExpression: "userId = :userId",
        ExpressionAttributeValues: {
            ":userId": req.headers.userid,
        },
    };

    result = await dynamoDB.getDynamo(params);
    return result;
};

exports.updateFavorites = async (req, res) => {
    await favoritesValidation.validateAssetType(req, res);
    await favoritesValidation.validateAssetTypeWithAssetId(req, res);

    let data = await isItemExists(req);

    if (data.Items.length == 0) {
        let error = new Error();
        error = {
            message: "Item couldn't be found",
            statusCode: 2,
            status: 404,
        };
        logger.error(JSON.stringify(error));
    }

    const params = {
        TableName: tableName,
        Key: {
            id: data["Items"][0].id,
            userId: req.headers.userid,
        },
        UpdateExpression: "SET #Duration = :duration",
        ConditionExpression:
            "userId = :user_id AND AssetId = :asset_id AND AssetType = :asset_type",
        ExpressionAttributeNames: { "#Duration": "Duration" },
        ExpressionAttributeValues: {
            ":duration": req.body.duration,
            ":user_id": req.headers.userid,
            ":asset_id": req.body.id,
            ":asset_type": req.body.asset_type,
        },
        ReturnValues: "UPDATED_NEW",
    };
    result = await dynamoDB.updateDynamo(params);
    return result;
};

exports.deleteFavorites = async (req, res) => {
    await favoritesValidation.validateAssetType(req, res);
    await favoritesValidation.validateAssetTypeWithAssetId(req, res);

    let data = await isItemExists(req);

    if (data.Items.length == 0) {
        let error = new Error();
        error = {
            message: "Item couldn't be found",
            statusCode: 2,
            status: 404,
        };
        logger.error(JSON.stringify(error));
    }

    const params = {
        TableName: tableName,
        Key: {
            id: data["Items"][0].id,
            userId: req.headers.userid,
        },
        ConditionExpression: "userId = :user_id AND AssetId = :asset_id",
        ExpressionAttributeValues: {
            ":user_id": req.headers.userid,
            ":asset_id": req.query.id,
        },
    };

    result = await dynamoDB.deleteDynamo(params);
    return result;
};

isItemExists = async (req) => {
    const getParams = {
        TableName: tableName,
        IndexName: indexName,
        KeyConditionExpression: "userId = :userId",
        FilterExpression: "AssetId = :assetId",
        ExpressionAttributeValues: {
            ":userId": req.headers.userid,
            ":assetId": req.body.id ? req.body.id : req.query.id,
        },
    };

    data = await dynamoDB.getDynamo(getParams);
    return data;
};
