const checkobject = require("./../../helpers/requestHelper");
const unknown = 99;
const logger = require('../../helpers/logger')
exports.verifyFavorites = (req, res, next) => {
    let body = req.body;
    if (checkobject.isEmpty(body)) {
        let error = new Error();
        error = { message: "No item in request", statusCode: 3, status: 404 };
        logger.error(JSON.stringify(error));
    } else {
        next();
    }
};

exports.validateInputs = async (req, res) => {
    try {
        if (
            isNullOrEmpty(req.body.id) ||
            isNullOrEmpty(req.body.asset_type) ||
            isNullOrEmpty(req.body.duration) ||
            isNullOrEmpty(req.body.date)
        ) {
            let error = new Error();
            error = {
                message: "Input missing in request",
                statusCode: 3,
                status: 400,
            };
            logger.error(JSON.stringify(error));
        }
        return true;
    } catch (error) {
        logger.error(JSON.stringify(error));
    }
};

exports.validateAssetType = async (req, res) => {
    if (req.body.asset_type == unknown) {
        let error = new Error();
        error = { message: "Invalid asset type", statusCode: 3, status: 404 };
        logger.error(JSON.stringify(error));
    }
    return true;
};

exports.validateAssetTypeWithAssetId = async (req, res) => {
    //let asset_type = req.body.asset_type ? req.body.asset_type : req.query.asset_type;
    let asset_type = req.body.hasOwnProperty("asset_type")
        ? req.body.asset_type
        : req.query.asset_type;
    let asset_id = req.body.id ? req.body.id : req.query.id;
    const regexp = new RegExp(`\\b-${asset_type}-\\b`);

    if (asset_id.match(regexp) == null) {
        let error = new Error();
        error = {
            message: "Asset type is not matched with the specified asset id",
            statusCode: 3,
            status: 404,
        };
        logger.error(JSON.stringify(error));
    }
    return true;
};

function isNullOrEmpty(input) {
    return input == null || input === "";
}
