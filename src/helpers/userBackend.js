const Utils = require("./utlis");
const auth = require("./auth")
const axiosHelper = require('./axiosCalls');
const utf8 = require('utf8');

class UserBackend{

    constructor(key, secret,domain) {
        this._key = key;
        this._secret = secret;
        this._maindomain = domain;
        this._utils = new Utils(key,secret,domain);
    }

    silentRegisterUser(uname,password,country="IN",ip=null,type='email',firstname='Guest',lastname='User',useragent="",additional=""){
        let userData={}
        if(uname.length > 0 && password.length > 0 && password.length >=6){
            userData = {"password":password,"first_name":firstname,"last_name":lastname,"mac_address":"","ip_address":"","registration_country":country,"additional":additional};
            userData[type] = uname;
        }
        if(ip && ip.length > 0){
            userData.ip_address = ip;
        }
        let pathUrl = `/v1/manage/customer/${type}`;
      
        let authKey =  auth.createAuthKeys(userData,pathUrl);
      
        let res= axiosHelper.call("POST",`${process.env.USER_API_BASE_URL}${pathUrl}`,userData,{
            "Content-Type":"application/json-patch+json",
            "Accept":"application/json",
            "Authorization":`${authKey}`,
            "Content-Length":(JSON.stringify(userData)).length,
            "User-Agent":`${useragent}`
          });
          return res;
        
    }

    getSilentUserToken(value,type='email'){
      let finalKey =   utf8.encode(this._key);
      let body = "";
      let query="";
      if(type == "email"){
        query=`email=${value.trim()}`;
      }else{
        query=`mobile=${value.trim()}`;
      }
      let path = `/v1/manage/customer/token?${query}`;
      let authKey =  auth.createAuthKeys(body,path,process.env.B2B_USERAPI_USERSECRET);
      return axiosHelper.call("GET",`${process.env.USER_API_BASE_URL}${path}`,"",{
        "Accept":"application/json",
        "Authorization":`${authKey}`
      });
    }
}


module.exports= UserBackend;