const _ = require("lodash");

module.exports = {
    error: function (message = 'ERROR', code = 400, httpCode = 400, meta = {}) {
        let status = {
            error_code: `${code}`,
            error_msg: message,
            http_code: httpCode,
            metaDetails: meta
        };
        if (_.isEmpty(meta)) delete (status.metaDetails);
        return status;
    },

    timeout: function (ms) {
        return new Promise(reject => setTimeout(reject, ms));
    }
}