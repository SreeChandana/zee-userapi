const businessLogic = require("../services/favorites/favoritesService");
const response = require("../helpers/responseHelper");
const redisCluster = require("../connections/redisConnection");
const config = require("../config/config");

// Get the favorites of the current user
exports.getFavorites = async (req, res, next) => {
    try {
        let result = await redisCluster.get(
            `${config.favorites.redis_prefix}${req.headers.userid}`
        );
        if (!result) {
            const resultAll = await businessLogic.getFavorites(req, res);
            result = JSON.stringify(resultAll);
            await redisCluster.set(
                `${config.favorites.redis_prefix}${req.headers.userid}`,
                result
            );
        }
        result = dataProjection(result);

        let apiResponse = response.generate(200, "Data found", result);
        res.status(200).send(apiResponse);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

// Add an item to the favorites of the current user
exports.addFavorites = async (req, res, next) => {
    try {
        const result = await businessLogic.addFavorites(req, res);
        await redisCluster.del(
            `${config.favorites.redis_prefix}${req.headers.userid}`
        );
        if (result) {
            let apiResponse = response.generate(
                1,
                "favorites was added successfully"
            );
            res.status(200).send(apiResponse);
        }
    } catch (err) {
        if (!err.statusCode) {
            err.message = "Item couldn't be added (DB insert failure)";
            err.statusCode = 500;
        }
        next(err);
    }
};

// Update an item in the favorites of the current user
exports.updateFavorites = async (req, res, next) => {
    try {
        const result = await businessLogic.updateFavorites(req, res);
        await redisCluster.del(
            `${config.favorites.redis_prefix}${req.headers.userid}`
        );
        if (result) {
            let apiResponse = response.generate(
                1,
                "favorites was updated successfully"
            );
            res.status(200).send(apiResponse);
        }
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

// Delete an item from the favorites of the current user
exports.deleteFavorites = async (req, res, next) => {
    try {
        const result = await businessLogic.deleteFavorites(req, res);
        await redisCluster.del(
            `${config.favorites.redis_prefix}${req.headers.userid}`
        );
        let apiResponse = response.generate(1, "Delete successful");
        if (result) {
            res.status(200).send(apiResponse);
        }
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

// Format the data as per response needed
dataProjection = (data) => {
    data = JSON.parse(data);
    data = data.Items.map(({ AssetId, AssetType, Duration, Date }) => ({
        id: AssetId,
        asset_type: AssetType,
        duration: Duration,
        date: Date,
    }));
    return data;
};
