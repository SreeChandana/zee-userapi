var fs = require('fs');
const fileService =  require('../services/files/filesService');
const logger = require('../helpers/logger');
const directoryPath =  "./public/";


// upload file to public folder
exports.uploaDumpFile = async (req, res, next) => {
    try {  
    
      const result = await fileService.uploaDumpFile(req, res, next);
      if(result){
        return res.status(200).send({
            "code":1,
            "message":`File uploaded successfully! `, 
            "path" : result
          })
      }
    }catch(err){
      logger.error(JSON.stringify(err))
      if (!err.statusCode) {
        err.message = "File couldn't be uploaded";
        err.statusCode = 500;
      }
      next(err);
    }
  }


  // get  all files in public folder, return list of files information
  exports.getListFiles = async (req, res, next) => {
    
  
    fs.readdir(directoryPath, function (err, files) {
      if (err) {
        res.status(500).send({
          message: "Unable to scan files!",
        });
      }
  
      let fileInfos = [];
  
      files.forEach((file) => {
        fileInfos.push({
          name: file,
          url: directoryPath + file,
        });
      });
  
      res.status(200).send(fileInfos);
    });
  };



// seed data into the table with given file path
exports.seedData = async (req, res, next) => {
    try {  
    
      const result = await fileService.seedData(req,res,next);
      if(result){
        return res.status(200).send({
            "code":1,
            "message":`${result} Records in ${req.body.table_name} Table` 
          })
      }
    }catch(err){
      logger.error(JSON.stringify(err))
      if (!err.statusCode) {
        err.message = "Item couldn't be added (DB insert failure)";
        err.statusCode = 500;
      }
      next(err);
    }
  }