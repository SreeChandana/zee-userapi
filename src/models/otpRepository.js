const {dynamoDB} = require('../connections/awsConnection');
const logger = require('../helpers/logger')
const config = require("../config/config");
var params={
    TableName:config.onetimepassword.table,
    AttributeDefinitions:[{
        AttributeName:'UserId',//partition key
        AttributeType:'S'
    }
],
    KeySchema:[{
        AttributeName:'UserId',
        KeyType:'HASH'
    }
],
   
    ProvisionedThroughput:{
        ReadCapacityUnits:10,
        WriteCapacityUnits:10
    },
}
dynamoDB.listTables({},function(err,data){
    let tables = null;
    if(data){
        tables= data.TableNames;
    }
    if(!data || tables.indexOf(params.TableName)==-1){
        dynamoDB.createTable(params,function(err,data){
            if(err){
                logger.error("Unable to create table.Error JSON"+JSON.stringify(err,null,2))
            }
            else{
                logger.info("Created table.Table description JSON:"+JSON.stringify(data,null,2))
            }
        });
    }
});