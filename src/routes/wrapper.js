const express = require("express");
const router = express.Router();
const wrapperController = require("../controllers/wrapperController");
const {setCorsHeaders} = require("../helpers/wrapperCorsHeaders");
const devceValidator = require('../middlewares/deviceValidator');
const healthcheckController = require("../controllers/healthcheckController");

// Please change routing method by self
//d
router.post("/manage/customer/:customer_id/password", devceValidator, wrapperController.changePassword);
//d
router.get("/user/renew", devceValidator, wrapperController.getRenewRefreshToken);
router.post("/user/silentRegistration", devceValidator, wrapperController.silentRegistration);
//d
router.get("/manage/customer/:customer_id", devceValidator, wrapperController.getUserLogin);
//d
router.post("/manage/customer/token", devceValidator, wrapperController.getUserToken);
//d
router.post("/user/recreatepasswordmobile", devceValidator, setCorsHeaders, wrapperController.recreatePwdLogin);


//d
router.post("/user/b2bSilentRegistration", devceValidator, setCorsHeaders, wrapperController.b2bSilentRegistration);

//d
router.post("/user/registration", devceValidator, wrapperController.userRegistration)
//d
router.post("/manage/customer", devceValidator, setCorsHeaders, wrapperController.updateUserDetails);
//d
router.post("/user/verifyotp", devceValidator, wrapperController.verifyOTP);

router.get("/healthCheck", healthcheckController.healthCheck);

// export router.
module.exports = router;