const express = require("express");
const router = express.Router();
const healthcheckController = require("../controllers/healthcheckController");

// Please change routing method by self
router.get("/healthCheck", healthcheckController.healthCheck);
// export router.
module.exports = router;